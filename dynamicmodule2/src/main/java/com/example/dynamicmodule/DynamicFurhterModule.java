package com.example.dynamicmodule;

public class DynamicFurhterModule implements IDynamicFurtherModule {
    @Override
    public String getText() {
        return "Loaded from dynamic module, this goes even around a second class instance. This is mighty!";
    }
}
