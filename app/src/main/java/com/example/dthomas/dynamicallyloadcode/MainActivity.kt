package com.example.dthomas.dynamicallyloadcode

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.dynamicmodule.ModuleLoader
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import kotlinx.android.synthetic.main.activity_main.*
import org.apache.commons.io.FileUtils
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadDexOnlineFile("dm.dex")

        text_view.text = "Downloading dex file..."
        textView2.text = loadDexFile("dm2.dex").text
    }

    fun sourceFile(name: String) = packageManager.getPackageInfo(packageName, 0)
            .applicationInfo.dataDir + "/files/" + name

    fun copyDexFile(name: String) = FileUtils.copyToFile(assets.open(name), File(sourceFile(name)))
            .let { File(sourceFile(name)) }

    fun loadDexFile(name: String) = ModuleLoader(cacheDir.absolutePath).load(copyDexFile(name))

    fun loadDexOnlineFile(name: String) {
        Fuel.download("https://tubify.org/$name")
                .fileDestination{ _, _ -> File(sourceFile(name)) }
                .progress { readBytes, totalBytes ->
                    val progress = readBytes.toFloat() / totalBytes.toFloat() * 100
                    Log.d(localClassName ,"Bytes downloaded $readBytes / $totalBytes ($progress %)")
                }
                .response { request, response, result ->
                    //Log.w("Request", request.toString())
                    //Log.w("Response", response.toString())
                    //Log.w("Result", result.toString())

                    result.success {
                        Log.d(localClassName, "Dex is downloaded")
                        runOnUiThread { text_view.text = ModuleLoader(cacheDir.absolutePath).load(File(sourceFile(name))).text }
                    }

                    result.failure {
                        runOnUiThread{ text_view.text = loadDexFile("dm_local.dex").text }
                    }
                }
    }
}
