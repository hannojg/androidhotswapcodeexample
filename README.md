# AndroidHotSwapCodeExample
How to load java code at run-time. Update parts of your app silently with out needing a full APK update.

Both library projects dynamicmodule and dynamicmodule2 are loaded at runtime. P
Dyanmicmodule is loaded from the web, while dynamicmodule2 is loaded from the assets directory.

This example shows how you add code to Android project that was not compiled into the original APK. This enables you to do stuff like "Over-the-air" updates. Currently, there doesn't exists any limation in the android framework to load classes like this. Please keep in mind that this is just a very brief showcase. 

There are tasks in dynamicmodule/2 called "copyDexToApp" which will "re-generate" the dex file and place them into the app modules assets directory.