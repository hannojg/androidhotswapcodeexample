package com.example.dynamicmodule;


public class DynamicModule implements IDynamicModule {

    @Override
    public String getText() {
        return "Dynamic Module 1 loaded dynamically, from local!";
    }
}
